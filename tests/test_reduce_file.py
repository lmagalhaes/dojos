import unittest
from src.reduce_file.reduce_file import ReduceFile
import os

class TestReduceFile(unittest.TestCase):

    def setUp(self):
        self.rd = ReduceFile()
        self.fixture = "{}{}".format(os.path.dirname(__file__), "/../fixtures/connection_list") 
        self.reduced = self.rd.reduce(self.fixture)

    def tearDown(self):
        self.rd = None
        self.fixture = ""

    def test_dict_must_contains_2_keys(self):
        self.assertEquals(2, len(self.reduced.keys()))

    def test_dict_must_contains_keys_equals(self):
        list_ = ['pagelevel_events.ktr', 'reports.ktr']
        self.assertEquals(list_, self.reduced.keys())

    def test_each_dict_element_must_contain_a_list_of_connections(self):
        for files in self.reduced.values():
            for f in files:
                self.assertTrue("connections" in f)

    def test_connections_list_must_not_have_an_empty_connection(self):
        for each in self.reduced.values():
            for conn in each['connections']:
                if not conn:
                    assert False

    def test_each_dict_element_must_contain_certain_keys(self):
        expected_keys = ["name", "server", "type"]
        sample_dict = self.reduced.values()[0]['connections'][0]
        keys = sample_dict.keys()   
        for expected_key in expected_keys:
            self.assertTrue(expected_key in keys)
