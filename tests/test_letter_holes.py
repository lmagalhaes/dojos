'''
Created on Jan 31, 2015

@author: lmagalhaes
'''
import unittest
from src.holes_on_letters.letter_holes import LetterHoles
class BaseTestCase(unittest.TestCase):

    def setUp(self):
        self.lh = LetterHoles()
    
    def tearDown(self):
        self.lh = None

class LetterHolesUpperCaseTest(BaseTestCase):

    def test_holes_attribute_equals_0_if_phrase_empty(self):
        self.assertEquals(0, self.lh.holes)
        
    def test_holes_attribute_equals_1_for_phrase_A(self):
        self.lh.phrase = "A"
        self.assertEquals(1, self.lh.holes)
    
    def test_holes_attribute_equals_2_for_phrase_B(self):
        self.lh.phrase = "B"
        self.assertEquals(2, self.lh.holes)
    
    def test_holes_attribute_equals_3_for_phrase_AB(self):
        self.lh.phrase = "AB"
        self.assertEquals(3, self.lh.holes)
        
    def test_holes_attribute_equals_3_for_phrase_ABC(self):
        self.lh.phrase = "ABC"
        self.assertEquals(3, self.lh.holes)
        
    def test_holes_attribute_equals_4_for_phrase_ABCD(self):
        self.lh.phrase = "ABCD"
        self.assertEqual(4, self.lh.holes)
    
    def test_holes_attribute_equals_0_for_phrase_CEFGHIJKLMNSTUVWXYZ(self):
        self.lh.phrase = "CEFGHIJKLMNSTUVWXYZ"
        self.assertEquals(0, self.lh.holes)
    
    def test_holes_attribute_equals_8_for_phrase_ABDOPQR(self):
        self.lh.phrase = "ABDOPQR"
        self.assertEquals(8, self.lh.holes)
        
    def test_holes_attribute_equals_12ForTHEQUICKBROWNFOXJUMPSOVERTHELAZYDOG(self):
        self.lh.phrase = "THEQUICKBROWNFOXJUMPSOVERTHELAZYDOG"
        self.assertEquals(12, self.lh.holes)


class LetterHolesCaseSensitive(BaseTestCase):
    
    def setUp(self):
        super(BaseTestCase, self).setUp()
        self.lh = LetterHoles(case_sensitive=True)

    def test_casesensitive_activate_by_constructor_param(self):
        self.assertTrue(self.lh.case_sensitive)

    def test_holes_attribute_equals_1_for_pharse_a(self):
        self.lh.phrase = 'a'
        self.assertEquals(1, self.lh.holes)
            
    def test_holes_attribute_equals_1_for_pharse_b(self):
        self.lh.phrase = 'b'
        self.assertEquals(1, self.lh.holes)
    
    def test_host_attribute_equals_2_for_bB(self):
        self.lh.phrase = 'bB'
        self.assertEquals(3, self.lh.holes)
        
    def test_host_attribute_equals_2_for_thequickbrownfoxjumpsoverthelazydog(self):
        self.lh.phrase = 'thequickbrownfoxjumpsoverthelazydog'
        self.assertEquals(13, self.lh.holes)
    
    def test_host_attribute_equals_13_for_ThEqUiCkBrOwNfOxJuMpSoVeRtHeLaZyDoG(self):
        self.lh.phrase = "ThEqUiCkBrOwNfOxJuMpSoVeRtHeLaZyDoG"
        self.assertEquals(13, self.lh.holes)
        

if __name__ == "__main__":
    unittest.main()
