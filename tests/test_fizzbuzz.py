from src.fizz_buzz.fizz_buzz import FizzBuzz
import unittest 

class BaseTestCase(unittest.TestCase):
    def setUp(self):
        self.fb = FizzBuzz()
    def tearDown(self):
        self.fb = None

class TestFizzbuzzReturnsFizz(BaseTestCase):
    
    def test_return_fizz_for_number_3(self):
        self.assertTrue("fizz" in self.fb.fizz_buzz(3))

    def test_return_fizz_for_number_6(self):
        self.assertTrue("fizz" in self.fb.fizz_buzz(6))

    def test_return_fizz_for_any_multiple_of_3(self):
        for i in range(1, 100):
            self.assertTrue("fizz" in self.fb.fizz_buzz(i*3))

class TestFizzBuzzReturnBuzz(BaseTestCase):

    def test_return_fizz_for_number_5(self):
        self.assertTrue("buzz" in self.fb.fizz_buzz(5))

    def test_return_fizz_for_number_10(self):
        self.assertTrue("buzz" in self.fb.fizz_buzz(10))

    def test_return_fizz_for_any_multiple_of_5(self):
        for i in range(1,100):
            self.assertTrue("buzz" in self.fb.fizz_buzz(i*5))

class TestFizzBuzzReturnsFizzBuzz(BaseTestCase):

    def test_return_fizzbuzz_if_number_is_multiple_of_3_and_5(self):
        self.assertEquals("fizzbuzz", self.fb.fizz_buzz(15))

class TestFizzBuzzReturnsInputNumber_is_not_multiple_of_3_or_five(BaseTestCase):

    def test_return_input_number_if_not_multiple_of_3(self):
        self.assertEqual(1, self.fb.fizz_buzz(1))
