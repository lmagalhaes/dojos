class FizzBuzz(object):
    
    def fizz_buzz(self, number_):
        response = ""

        if number_ % 3 == 0:
            response = "fizz"
        if number_ % 5 == 0:
            response += "buzz"

        if not response:    
            response = number_
        
        return response

    
