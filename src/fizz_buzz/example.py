from fizzbuzz.fizzbuzz import FizzBuzz

if __name__ == "__main__":
    fb = FizzBuzz()
    lst = [ fb.fizz_buzz(n) for n in xrange(1,100)]
    print lst
