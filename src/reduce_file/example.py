from reduce_file import ReduceFile
import os, sys

def write_to_file(f_, str_):
    f_.write("{}{}".format(str_, os.linesep))

def execute(connection_file, filename):
    rf = ReduceFile()
    items = rf.reduce(connection_file)
    if of:
        with open(filename, "w") as f:
            write_to_file(f, "#{}{}".format(filename, os.linesep))
            keys = items.keys()
            for key in keys:
                write_to_file(f, "File: {}".format(key))
                write_to_file(f, "Connections")
                for num, conn in enumerate(items[key]['connections']):
                    write_to_file(f, "\tConnection {}".format(num+1))
                    attrs = conn.keys()
                    for attr in attrs:
                        write_to_file(f, "\t\t{}: {!r}".format(attr, conn[attr]))
                    write_to_file (f, "")   

if __name__ == "__main__":
    of = None
    if len(sys.argv) > 1:
        of = sys.argv[1]
        
    connection_file = "{}".format(os.path.join(os.path.abspath(os.path.dirname(__file__)), "connection_list"))
    execute(connection_file, of)
