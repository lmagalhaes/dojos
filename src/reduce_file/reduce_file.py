import re

class ReduceFile(object):

    def __init__(self):
        self.pattern = r"<(?P<tag>[A-Z0-9]+).*?>(?P<content>.*)</(?P=tag)>"

    def reduce(self, file_):
        dict_ = {}
        for name, conn in self.connection_generator(file_):
            if conn:
                if not name in dict_:
                    dict_[name] = { "connections": [conn]}
                else:
                    dict_[name]["connections"].append(conn)

        return dict_

    def connection_generator(self, file_):
        dict_ = {}
        prev_file = ""
        with open(file_, 'r') as f:
            while True:
                try:
                    line = f.next()
                    if re.match("--", line):
                        continue

                    fname = re.split(" +", line)[0][:-1].split("/")[-1]

                    if not fname in  dict_:
                        dict_[fname] = {}

                    if self.test_attribute(line):
                        attr, value = self.get_tag_attribute_value(line)
                        if attr != "connection":
                            dict_[fname][attr] = value

                    if prev_file != fname or "connection" in line:
                        prev_file = fname
                        yield fname, dict_[fname]
                        dict_[fname] = {}
                    
                except StopIteration:
                    break
   
    def test_attribute(self, string):
        return True if re.search(self.pattern, string, re.I) else False

    def convert_html_entities(self, phrase):
        for char in set([ char for char in re.findall("&#x(?P<char>[a-f0-9]+);", phrase )]):
            phrase = phrase.replace("&#x{};".format(char), unichr(int(char,16)))
        return phrase

    def get_tag_attribute_value(self, string):
        resp = re.search(self.pattern, string, flags=re.I).groupdict()
        return resp['tag'], self.convert_html_entities(resp['content'])
