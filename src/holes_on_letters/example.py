from letter_holes import LetterHoles

text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer facilisis pellentesque velit. Cras eget tellus consequat, dapibus est eu, lobortis magna. Sed faucibus tempor arcu, quis efficitur lorem pharetra in. Sed scelerisque posuere lacinia. Phasellus eu faucibus felis. Sed rutrum erat nec risus tincidunt tempus. Ut vitae sollicitudin urna, a malesuada nisl. Donec sapien nulla, ultrices non magna a, posuere molestie mauris. Nam dictum auctor quam, ut sagittis nibh cursus id. Maecenas tristique magna ut purus lacinia efficitur."

case_sensitive = LetterHoles(case_sensitive=True)
case_sensitive.phrase = text
print "The case-sensitive phrase %s has %d holes" % (text, int(case_sensitive.holes))
del case_sensitive

print
print

case_insensitive = LetterHoles()
case_insensitive.phrase = text
print "The case-insensitive phrase %s has %d holes" % (text.upper(), int(case_insensitive.holes))
del case_insensitive