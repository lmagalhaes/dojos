class LetterHoles(object):
    '''
    Count the holes left by the letter on a phrase
    @see dojopuzzles.com - http://dojopuzzles.com/problemas/exibe/buracos-nas-letras/#
    
    The whole phrase will be converted to upper case unless case_sensitive flag is True
    '''
    scores_by_letter = {'A' : 1, 'B' : 2, 'D' : 1, 'O' : 1, 'P' : 1, 'Q' : 1, 'R': 1,
                        'a' : 1, 'b' : 1, 'd':1, 'e': 1, 'g': 1, 'o': 1, 'p': 1, 'q': 1}
    
    phrase_value = ""
    
    def __init__(self, **kwargs):
        self.case_sensitive = kwargs.get('case_sensitive', False)
        self.holes = 0
        self.phrase = ""
        
    def compute_holes(self):
        count = 0
        phrase = self.phrase
        
        if self.case_sensitive is False:
            phrase = self.phrase.upper()
            
        for char in phrase:
            count += self.scores_by_letter.get(char, 0)
        
        self.holes = count
    
    def get_phrase(self):
        return self.phrase_value
    
    def set_phrase(self, phrase):
        self.phrase_value = phrase
        self.compute_holes()
        
    phrase = property(get_phrase, set_phrase)
    